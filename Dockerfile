FROM golang:1.15.1
ENV GO111MODULE=on

# RUN mkdir -p ~/go/config
# RUN mkdir -p /usr/local/go/src/bitbucket.org/icampusjjc/successcenter-go
# COPY ./ /usr/local/go/src/bitbucket.org/icampusjjc/successcenter-go
# WORKDIR /usr/local/go/src/bitbucket.org/icampusjjc/successcenter-go

# Download all the dependencies
RUN go mod download

EXPOSE 3600
