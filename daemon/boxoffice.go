package daemon

import (
	"fmt"
	"github.com/gocolly/colly"
	"movies-go/dao"
	"movies-go/models/dbd"
	"movies-go/tools"
	"strconv"
	"strings"
	"time"
)

/* This Scraper gathers Title, Studio and Release Date as well as rank from BoxOfficeMojo */
var baseUrl = "https://www.boxofficemojo.com"
var movieMap map[string]dbd.Movie

// ScrapeBoxOfficeFromTo ScrapeBoxOffice used by scrapers
func ScrapeBoxOfficeFromTo(firstDataYear int, lastDataYear  int){
	for i := lastDataYear; i >= firstDataYear; i-- {
		movieMap = map[string]dbd.Movie{}
		message := "[ Importing Year: " + strconv.Itoa(i) + " ]"
		details := strconv.Itoa(i)
		tools.LogIt("BOM IMPORT", message, details)
		processURLListForYear(strconv.Itoa(i))
		fmt.Println("Movie Map", len(movieMap))
		saveMovies()
	}
}

func getURLListForYear(year string) []string {
	var uRLsToProcess []string

	for i := 1; i <= 12; i++ {
		url := baseUrl + "/month/" + strings.ToLower(time.Month(i).String()) + "/" + year
		uRLsToProcess = append(uRLsToProcess, url)
	}

	return uRLsToProcess
}

func processURLListForYear(year string){
	uRLsToProcess := getURLListForYear(year)

	for _, url := range uRLsToProcess {
		processBoxOfficePage(url, year)
	}
}

func buildMovieMap(movies []dbd.Movie) map[string]dbd.Movie {
	// Build a config map:
	confMap := map[string]dbd.Movie{}
	for _, m := range movies {
		key := ""
		for _, e := range m.ExternalSources {
			if e.Source == "BoxOfficeMojo" {
				key = e.ExternalID
				break
			}
		}
		confMap[key] = m
	}
	return confMap
}

func addNonDuplicateMovies(mMap map[string]dbd.Movie) {
	for k, mov := range mMap {
		if _, ok := movieMap[k]; !ok {
			movieMap[k] = mov
		}
	}
}

func saveMovies() {
	var movies []dbd.Movie
	for k, movie := range movieMap {
		m := updateMovieObject(movie, k)
		movies = append(movies, *m)
		fmt.Println( m.ID, "UPDATING:", m.Title)

	}
	dao.CreateMovieBulk(movies)
}

func processBoxOfficePage(url string, year string){
	var movies []dbd.Movie
	// var updateMovies []dbd.Movie
	message := "[ Scraping: " + url + " ]"
	details := year
	tools.LogIt("BOM PROCESS PAGE", message, details)

	c := colly.NewCollector(
		colly.AllowedDomains("www.boxofficemojo.com"),
		colly.DetectCharset(),
	)
	c.DetectCharset = true
	err := c.Limit(&colly.LimitRule{
		DomainGlob:  "*boxofficemojo.*",
		Parallelism: 3,
		Delay:      1 * time.Second,
	})

	if err != nil {
		message := "[ Error Scraping: " + url + " ]"
		details := err.Error()
		tools.LogIt("BOM ERROR", message, details)
	}

	c.OnHTML("tr td:nth-of-type(1)", func(e *colly.HTMLElement) {
		score, err := strconv.Atoi(e.Text)
		if err != nil {
			return
		}

		var movie dbd.Movie
		rankElement := e.DOM

		titleElement := rankElement.Next()
		movie.Title = titleElement.Find("a").Text()
		link, fnd := titleElement.Find("a").Attr("href")
		var endIndex =  strings.Index(link, "/?")

		var boxId = ""
		if fnd {
			boxId = link[9:endIndex]
		} else {
			boxId = titleElement.Text()
		}
		// tools.LogIt("BoxOfficeMojo", boxId, "boxId")

		t1 := titleElement.Next().Next().Next()
		grossElement := t1.Next()
		// tools.LogIt("BoxOfficeMojo", grossElement.Text(), "GrossElement")
		grossTheatersElement := grossElement.Next()
		// tools.LogIt("BoxOfficeMojo", grossTheatersElement.Text(), "GrossTheater")
		grossTotalElement := grossTheatersElement.Next()
		// tools.LogIt("BoxOfficeMojo", grossTotalElement.Text(), "OpeningElement")
		releasedElement := grossTotalElement.Next()
		date, err := time.Parse("Jan 2, 2006", releasedElement.Text() + ", " + year)

		movie.Released = date
		// tools.LogIt("BoxOfficeMojo", movie.Released.String(), "Released")
		studioElement := releasedElement.Next()
		movie.Studio = strings.TrimSpace(studioElement.Text())
		// tools.LogIt("BoxOfficeMojo", movie.Studio, "StudioElement")
		var externalSource dbd.ExternalSource
		externalSource.Source = "BoxOfficeMojo"
		externalSource.ExternalID = boxId
		externalSource.Score = score
		movie.ExternalSources = append(movie.ExternalSources, externalSource)

		movies = append(movies, movie)
	})

	c.OnScraped(func(r *colly.Response) {
		addNonDuplicateMovies(buildMovieMap(movies))
	})

	err = c.Visit(url)
	if err != nil {
		message := "[ Error Visiting: " + url + " ]"
		details := err.Error()
		tools.LogIt("BOM ERROR", message, details)
	}
}


func updateMovieObject(movie dbd.Movie, boxOfficeID string) *dbd.Movie {
	m := dao.ReadMovieByBoxOfficeID(boxOfficeID)

	if m != nil {
		if match(*m, movie) {
			m.Released = movie.Released
			m.Studio = movie.Studio
			m.Title = movie.Title
			m.ExternalSources = movie.ExternalSources
			dao.UpdateMovie(*m)
		}
	}

	return m
}

func match(old dbd.Movie, new dbd.Movie) bool {
	if old.Studio != new.Studio { return false }
	if old.Title != new.Title { return false }
	if old.Released != new.Released { return false }
	return true
}
