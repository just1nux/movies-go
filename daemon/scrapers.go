package daemon

import (
	"movies-go/tools"
	"time"
)

func Start() {
	StartScrapersNonthread()
	// StartScrapers()
}

func StartScrapers() {
	go ScrapeBoxOfficeFromTo(time.Now().Year(), time.Now().Year())
	go ScrapeJustWatch()
}

func StartScrapersNonthread() {
	tools.LogIt("BOM SCRAPER", "START", "Box Office Mojo Scraping Started")
	// ScrapeBoxOfficeFromTo(time.Now().Year(), time.Now().Year())
	tools.LogIt("BOM SCRAPER", "FINISH", "Box Office Mojo Scraping Finished")

	tools.LogIt("JW SCRAPER", "START", "Just Watch Scraping Started")
	ScrapeJustWatch()
	tools.LogIt("JW SCRAPER", "FINISH", "Just Watch Scraping Started")
}


