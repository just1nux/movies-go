package daemon

import (
	"encoding/json"
	"io/ioutil"
	"movies-go/dao"
	"movies-go/models/dbd"
	"movies-go/tools"
	"net/http"
	"strconv"
)

var justBaseUrl = "https://apis.justwatch.com/content/"

func ScrapeJustWatch() {
	getProviderList()
	// loopPages()
}

func getProviderList() {
	jwUrl := justBaseUrl + "providers/locale/en_US"
	res, err := http.Get(jwUrl)

	if err != nil {
		message := "[ Error Getting Provider for JSON ]"
		details := err.Error()
		tools.LogIt("JustWatch", message, details)
	} else {
		body, err := ioutil.ReadAll(res.Body)

		if err != nil {
			message := "[ Error Reading Provider JSON ]"
			details := err.Error()
			tools.LogIt("JustWatch", message, details)
		} else {
			var providers []dbd.Provider
			err = json.Unmarshal(body, &providers)
			providers = replaceOldData(providers)

			var created = dao.CreateProviderBulk(providers)

			message := "[" + created + " Providers ]"
			details := created
			tools.LogIt("JustWatch", message, details)
		}
	}
}

func replaceOldData(providers []dbd.Provider) []dbd.Provider {
	for i, v := range providers {
		vw := strconv.Itoa(v.IdJW)
		externalSource := dbd.ExternalSource{
			ExternalID:     vw,
			Source: "JustWatch",
			Score: -1,
		}

		var lookedUp dbd.Provider = dao.GetProviderBySource(externalSource)
		lookedUp.ClearName = v.ClearName
		lookedUp.IconURL = v.IconURL
		lookedUp.ShortName = v.ShortName
		lookedUp.TechnicalName = v.TechnicalName
		lookedUp.Slug = v.Slug
		lookedUp.MonetizationTypes = v.MonetizationTypes
		lookedUp.IdJW = int(v.ID)
		lookedUp.ExternalSources = v.ExternalSources
		lookedUp.PresentationTypes = v.PresentationTypes
		providers[i] = lookedUp
	}
	return providers
}
/*
func loopPages(){

	ask := dbd.MediaAsk{
		PageSize: 100,
	}

	pageCount := dao.MovieCount()/ask.PageSize

	for i := 1; i <= pageCount; i++ {
		ask.Page = i
		fmt.Println("Page: ", i, "of", pageCount)
		loopThroughPageOfMovies(dao.MovieIndex(ask))
	}
}

func loopThroughPageOfMovies(movies []models.Movie){
	for _, movie := range movies {
		media := searchJustWatchMedia(movie)
		if media != nil {
			updateMovieFromMedia(movie, *media)
		}
		//time.Sleep(250 * (time.Millisecond))
	}
}

func searchJustWatchMedia(movie models.Movie) *models.JWMedia {
	media, err := getMediaItemFromJustWatch(movie)
	if err != nil {
		message := "[ Error: No Media Found for <" + movie.Id.String() + "> '" + movie.Title + "' ]"
		details := err.Error()
		logIt("JustWatchFail", message, details)
	}
	return media
}

func convertToPayload(body []byte) models.Payload {
	var returnPayload models.Payload
	err := json.Unmarshal(body, &returnPayload)

	if err != nil {
		message := "[ Error Converting Payload JSON ]"
		details := err.Error()
		logIt("JustWatch", message, details)
	}
	return returnPayload
}

func getMediaItemFromJustWatch(movie models.Movie) (*models.JWMedia, error) {
	paramsJSON := buildParams(movie)
	justWatchUrl := justBaseUrl + "titles/en_US/popular?body=" + paramsJSON
	res, err := http.Get(justWatchUrl)

	// https://apis.justwatch.com/content/titles/en_US/new?genres=act,cmy,crm,doc,drm,fnt,hrr,hst,msc,rly,scf,war&providers=amp,atp,cbs,dnp,fxn,hbm,hlu,nfx,pct,sho&release_year_from=2020

	if err != nil {
		message := "[ Error Searching Movie for JSON ]"
		details := err.Error()
		logIt("JustWatch", message, details)
	} else {

		body, err := ioutil.ReadAll(res.Body)

		if err != nil {
			message := "[ Error Reading Payload JSON ]"
			details := err.Error()
			logIt("JustWatch", message, details)
		}

		payload := convertToPayload(body)
		if len(payload.Items) > 0 {
			return &payload.Items[0], nil
		}
	}
	return nil, errors.New("ItemsNotFound")
}

func buildParams(movie models.Movie) string {
	var params = models.Payload{
		ContentTypes : []string{"movie"},
		Page:1,
		PageSize:20,
		Query: movie.Title,
		ReleaseYearFrom: movie.Released.Year()-1,
		ReleaseYearUntil: movie.Released.Year()+1,
	}
	paramsJson, err := json.Marshal(params)
	if err != nil {
		fmt.Printf("Error: %s", err)
		return err.Error()
	}
	return url.QueryEscape(string(paramsJson))
}

func updateMovieFromMedia(movie models.Movie, media models.JWMedia){
	firstImport := true
	for _, source := range movie.ExternalSources {
		if source.Source == "JustWatch"{
			firstImport = false
			break
		}
	}

	if firstImport {
		externalSource := createExternalSource(media.Id)
		movie.ExternalSources = append(movie.ExternalSources, externalSource)
	}

	movie.Description = media.ShortDescription
	var offers []models.Offer
	for _, mediaOffer := range media.Offers {
		offers = append(offers, mediaOfferToMovieOffer(mediaOffer))
	}
	movie.Offers = offers

	score := smetrics.Jaro(stripYear(media.Title, media.OriginalReleaseYear), stripYear(movie.Title, movie.Released.Year()))
	if score > 0.80 {
		message := "Updating '" + stripYear(movie.Title, movie.Released.Year()) + "' from JustWatch Data: " + strconv.Itoa(media.Id) + " '" + stripYear(media.Title, media.OriginalReleaseYear) + "'"
		details := movie.Id.String() + " Score:" + strconv.FormatFloat(score*100, 'f', -1, 64)
		logIt("JustWatch", message, details)

	} else {
		message := "Not Updating '" + stripYear(movie.Title, movie.Released.Year()) + "' from JustWatch Data: " + strconv.Itoa(media.Id) + " '" + stripYear(media.Title, media.OriginalReleaseYear) + "'"
		details := movie.Id.String() + " Score:" + strconv.FormatFloat(score*100, 'f', -1, 64)
		logIt("JustWatch", message, details)
		//remove all offers and description and remove JustWatch source
		movie.Offers = nil
		movie.Description = ""
		for i, v := range movie.ExternalSources {
			if v.Source == "JustWatch" {
				movie.ExternalSources = append(movie.ExternalSources[:i], movie.ExternalSources[i+1:]...)
			}
		}
	}

	dao.MovieUpdate(movie)
}

func mediaOfferToMovieOffer(mediaOffer models.JWOffer) models.Offer {
	offer := models.Offer{
		MonetizationType: mediaOffer.MonetizationType,
		ProviderId: mediaOffer.ProviderId,
		RetailPrice: mediaOffer.RetailPrice,
		PresentationType: mediaOffer.PresentationType,
	}
	return offer
}

func createExternalSource(jwId int) models.ExternalSource {
	externalSource := models.ExternalSource{
		Id:     strconv.Itoa(jwId),
		Source: "JustWatch",
		Score: -1,
	}
	return externalSource
}

func stripYear(title string, year int) string {
	returnTitle := title
	if strings.Index(title, " (" + strconv.Itoa(year) + ")") >= 0 {
		returnTitle = title[:strings.Index(title, " (" + strconv.Itoa(year) + ")")]
	} else if strings.Index(title, "(" + strconv.Itoa(year-1) + ")") >= 0 {
		returnTitle = title[:strings.Index(title, " (" + strconv.Itoa(year-1) + ")")]
	} else if strings.Index(title, "(" + strconv.Itoa(year+1) + ")") >= 0 {
		returnTitle = title[:strings.Index(title, " (" + strconv.Itoa(year+1) + ")")]
	}
	return returnTitle
}
*/
