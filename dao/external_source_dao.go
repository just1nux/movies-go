package dao

import (
	"fmt"
	"movies-go/models/dbd"
)

func CreateExternalSource(externalSource *dbd.ExternalSource) (*dbd.ExternalSource, error) {

	result := Db.Create(&externalSource)
	if result.Error != nil {
		fmt.Println(result.Error)
		return nil, result.Error
	} else if result.RowsAffected > 0 {
		fmt.Println(result.RowsAffected, "Movie Added")
	}

	return externalSource, nil
}

func ReadExternalSourceByID(externalSourceID int) (*dbd.ExternalSource, error) {
	var externalSource dbd.ExternalSource

	Db.Where(externalSourceID).First(&externalSource)

	return &externalSource, nil
}


func ReadExternalSourceIndex() (*[]dbd.ExternalSource, error) {
	var externalSources []dbd.ExternalSource

	Db.Limit(100).Find(&externalSources)

	return &externalSources, nil
}

func externalSourceMigrate() error {

	err := Db.AutoMigrate(&dbd.ExternalSource{})
	if err != nil {
		fmt.Println(err.Error())
		return err
	}
	return nil
}
