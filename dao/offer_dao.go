package dao

import (
	"fmt"
	"movies-go/models/dbd"
)

func CreateOffer(offer *dbd.Offer) (*dbd.Offer, error) {

	result := Db.Create(&offer)
	if result.Error != nil {
		fmt.Println(result.Error)
		return nil, result.Error
	} else if result.RowsAffected > 0 {
		fmt.Println(result.RowsAffected, "Offer Added")
	}

	return offer, nil
}

func ReadOfferByID(offerID int) (*dbd.Offer, error) {
	var offer dbd.Offer

	Db.Where(offerID).First(&offer)

	return &offer, nil
}


func ReadOfferIndex() (*[]dbd.Offer, error) {
	var offers []dbd.Offer

	Db.Limit(100).Find(&offers)

	return &offers, nil
}

func offerMigrate() error {

	err := Db.AutoMigrate(&dbd.Offer{})
	if err != nil {
		fmt.Println(err.Error())
		return err
	}
	return nil
}
