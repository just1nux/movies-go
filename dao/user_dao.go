package dao

import (
	"database/sql"
	"fmt"
	"gorm.io/gorm"
	"movies-go/models/config"
	"movies-go/models/dbd"
	"strconv"
)

func CreateUser(userObject *dbd.User) (*dbd.User, error) {

	var u dbd.User
	if err := Db.Where(dbd.User{CanvasID: userObject.CanvasID}).First(&u).Error; err != nil && err.Error()=="record not found" {
			fmt.Println("RECORD NOT FOUND")
			result := Db.Create(&userObject)
			if result.Error != nil {
				fmt.Println(result.Error)
				message := new(config.Log)
				message.LogEvent = "COLLECTORERROR"
				message.EventType = "User Create: " + strconv.Itoa(int(userObject.ID))
				message.Details = result.Error.Error()
				fmt.Println(message)
				_, _ = CreateLog(message)
				return nil, nil
			}
			fmt.Println(result.RowsAffected, "User Added")


	}else{
		userObject.ID = u.ID

		if u.Role == "admin" {
			userObject.Role = u.Role
		}

		userObject.DeletedAt = gorm.DeletedAt(sql.NullTime{})

		fmt.Println("U:", userObject.Role)
		fmt.Println("UO:", u.Role)
		// fmt.Println("U:", u.LastAccess)
		// fmt.Println("UO:", userObject.LastAccess)

		if userObject.LastAccess.Before(u.LastAccess) {
			userObject.LastAccess = u.LastAccess
		}

		result := Db.Save(userObject)
		if result.Error != nil {
			fmt.Println(result.Error)
			return nil, nil
		}
		fmt.Println(result.RowsAffected, "User Updated", userObject)
	}

	return userObject, nil
}

func ReadUserByID(UserID string) (*dbd.User, error) {
	user := new (dbd.User)

	Db.Where(UserID).First(&user)

	return user, nil
}

func ReadUserIndex() (*[]dbd.User, error) {
	var users []dbd.User

	Db.Limit(100).Find(&users)

	return &users, nil
}

func userMigrate() error {

	err := Db.AutoMigrate(&dbd.User{})
	if err != nil {
		fmt.Println(err)
		return err
	}
	return nil
}
