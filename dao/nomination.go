package dao

import (
	"fmt"
	"movies-go/models/dbd"
)

func CreateNomination(nomination *dbd.Nomination) (*dbd.Nomination, error) {
	result := Db.Create(&nomination)
	if result.Error != nil {
		fmt.Println(result.Error)
		return nil, result.Error
	} else if result.RowsAffected > 0 {
		fmt.Println(result.RowsAffected, "Nomination Added")
	}

	return nomination, nil
}

func ReadNominationByID(nominationID int) (*dbd.Nomination, error) {
	var nomination dbd.Nomination

	Db.Where(nominationID).First(&nomination)

	return &nomination, nil
}


func ReadNominationIndex() (*[]dbd.Nomination, error) {
	var nominations []dbd.Nomination

	Db.Limit(100).Find(&nominations)

	return &nominations, nil
}

func nominationMigrate() error {
	err := Db.AutoMigrate(&dbd.Nomination{})
	if err != nil {
		fmt.Println(err)
		return err
	}
	return nil
}
