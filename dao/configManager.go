package dao

import (
	"encoding/json"
	"io/ioutil"
	"movies-go/models/config"
)

func saveConfig(c config.Configuration, filename string) error {
	bytes, err := json.MarshalIndent(c, "", "  ")
	if err != nil {
		return err
	}

	return ioutil.WriteFile(filename, bytes, 0644)
}

func loadConfig(filename string) (config.Configuration, error) {
	bytes, err := ioutil.ReadFile(filename)
	if err != nil {
		return config.Configuration{}, err
	}

	var c config.Configuration
	err = json.Unmarshal(bytes, &c)
	if err != nil {
		return config.Configuration{}, err
	}

	return c, nil
}

func createMockConfig() config.Configuration {
	return config.Configuration{
		DB: config.DB{
			Server:   "db.server.com",
			Login:    "Login",
			Password: "Password",
			Database: "Database",
		},
	}
}

