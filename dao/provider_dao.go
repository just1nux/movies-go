package dao

import (
	"fmt"
	"gorm.io/gorm/clause"
	"movies-go/models/dbd"
)

func CreateProvider(provider *dbd.Provider) (*dbd.Provider, error) {

	providerLoaded, _ := ReadProviderByJWID(provider.IdJW)

	if providerLoaded == nil  {
		fmt.Println("CREATE", provider)
		err := Db.Create(&provider)
		if err.Error != nil {
			fmt.Println(err.Error)
			return nil, err.Error
		}
	}

	return providerLoaded, nil
}

func ReadProviderByID(providerID int) (*dbd.Provider, error) {
	var provider dbd.Provider
	Db.Where(providerID).First(&provider)
	return &provider, nil
}

func ReadProviderByJWID(jwID int) (*dbd.Provider, error) {
	var provider dbd.Provider
	fmt.Println(jwID)
	err := Db.Where("id_jw = ?", jwID).First(&provider)
	if err.Error != nil {
		fmt.Println(err.Error)
		return nil, err.Error
	}
	return &provider, nil
}

func ReadProviderIndex() (*[]dbd.Provider, error) {
	var providers []dbd.Provider
	Db.Limit(100).Find(&providers)
	return &providers, nil
}

func providerMigrate() error {
	err := Db.AutoMigrate(&dbd.Provider{})
	if err != nil {
		fmt.Println(err)
		return err
	}
	return nil
}

func CreateProviders(providers []dbd.Provider) string {
	for _, provider := range providers {
		_, err := CreateProvider(&provider)
		if err != nil {
			fmt.Println(err.Error())
		}
	}
	return "Complete"
}

func GetProviderBySource(source dbd.ExternalSource) dbd.Provider {
	var provider dbd.Provider
	return provider
}

func CreateProviderBulk(providers []dbd.Provider) string {
	err := Db.Clauses(clause.OnConflict{
		UpdateAll: true,
	}).Create(&providers)

	if err == nil {
		return "Inserted"
	} else {
		fmt.Println(err.Error)
		return "Error"
	}
}
