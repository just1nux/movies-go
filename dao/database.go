package dao

import (
	"fmt"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
	"log"
	"movies-go/models/config"
	"os"
	"strconv"
	"time"
)

const (
	configFilename = "/var/go/config/movies.json"
)

var Config config.Configuration
var Db *gorm.DB

func GetConfigFromFile(){
	// Get config file from file system
	if _, err := os.Stat(configFilename); err == nil {
		Config, err = loadConfig(configFilename)
		if err != nil {
			panic(err)
		}
	} else if os.IsNotExist(err) {
		err := saveConfig(createMockConfig(), configFilename)
		if err != nil {
			log.Printf("Check Directory location and Permissions.\n")
			panic(err)
		}
		Config, err = loadConfig(configFilename)
		if err != nil {
			panic(err)
		}
	} else {
		log.Printf("File may or may not exist. See err for details.\n")
	}
	printDetails()
}

func printDetails() {
	fmt.Println(">> CONFIG")
	fmt.Println("  * DB")
	fmt.Println("    -- ", Config.DB.Server)
	fmt.Println("    -- ", Config.DB.Database)
	fmt.Println("  * EVENTS")
	for _, event := range Config.Events {
		fmt.Println("      ++", event.EventName)
		fmt.Println("      ++", strconv.Itoa(event.RunHour) + ":" + strconv.Itoa(event.RunMinute))
		fmt.Println("      ++ Repeat every ", event.RepeatEveryHours, " hours")
	}
	fmt.Println(">> CONFIG")
	fmt.Println()
}
/*
func StartDB() {
	newLogger := logger.New(
		log.New(os.Stdout, "\r\n", log.LstdFlags), // io writer
		logger.Config{
			SlowThreshold: time.Second,   // Slow SQL threshold
			LogLevel:      logger.Silent, // Log level
			Colorful:      false,         // Disable color
		},
	)
	//username:password@protocol(address)/dbname?param=value
	// dsn := "movies:Movie$@tcp(192.168.1.25:3306)/movies?charset=utf8mb4&parseTime=True&loc=Local"

	dsn := Config.DB.Login + ":" + Config.DB.Password + "@tcp(" +  Config.DB.Server + ":3306)/" + Config.DB.Database
	fmt.Println(dsn)
	db, err := gorm.Open(mysql.Open(dsn), &gorm.Config{Logger: newLogger})

	if err != nil {
		fmt.Println("ERROR:", err)
	}

	stDB, err2 := db.DB()
	if err2 != nil {
		fmt.Println("ERROR:", err)
	} else {
		stDB.SetConnMaxLifetime(time.Hour * 9)
	}
}

func OpenDB() (*gorm.DB, error) {
	stDB, err := db.DB()

	if err != nil {
		fmt.Println("ERROR:", err)
		StartDB()
	}

	err = stDB.Ping()

	if err != nil {
		fmt.Println("PING FAILED", err)
		StartDB()
	}
	return db, nil
}*/

func Migrate() error {
	OpenDB()

	migrations := []func() error{
		logMigrate,
		userMigrate,
		externalSourceMigrate,
		providerMigrate,
		offerMigrate,
		nominationMigrate,
		movieMigrate,
	}

	for _, m := range migrations {
		err := m()
		if err != nil {
			fmt.Println(err)
			return err
		}
	}

	return nil
}


func OpenDB() *gorm.DB {
	Db = LaunchDB()
	return Db
}

func LaunchDB() *gorm.DB {
	newLogger := logger.New(
		log.New(os.Stdout, "\r\n", log.LstdFlags), // io writer
		logger.Config{
			SlowThreshold: time.Second,   // Slow SQL threshold
			LogLevel:      logger.Silent, // Log level
			Colorful:      false,         // Disable color
		},
	)

	dsn := Config.DB.Login + ":" + Config.DB.Password + "@tcp(" +  Config.DB.Server + ":3306)/" + Config.DB.Database + "?parseTime=true"
	Db, err := gorm.Open(mysql.Open(dsn), &gorm.Config{Logger: newLogger})

	if err != nil {
		fmt.Printf("Error connecting to database : error=%v\n", err)
		return nil
	}
	stDB, err1 := Db.DB()
	if err1 != nil {
		fmt.Printf("Error getting DB connection : error=%v\n", err1)
		return nil
	}

	err = stDB.Ping()

	if err != nil {
		fmt.Printf("Error failing Ping test: error=%v\n", err)
	}
	fmt.Println("PING COMPLETE");
	stDB.SetConnMaxLifetime(time.Hour * 9)
	return Db
}
