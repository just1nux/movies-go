package dao

import (
	"fmt"
	"gorm.io/gorm/clause"
	"movies-go/models/dbd"
)

func CreateMovie(movie *dbd.Movie)  {
	result := Db.Create(&movie)
	if result.Error != nil {
		fmt.Println(result.Error)
		return
	} else if result.RowsAffected > 0 {
		fmt.Println(result.RowsAffected, "Movie Added")
	}
	return
}

func ReadMovieByID(movieID int) (*dbd.Movie, error) {
	var movie dbd.Movie
	Db.Where(movieID).First(&movie)
	return &movie, nil
}



func ReadMovieIndex() (*[]dbd.Movie, error) {
	var movies []dbd.Movie

	err := Db.Debug().Limit(100).Find(&movies)
	if err != nil {
		message := "[ Error Getting Movie Index ]"
		details := err.Error
		fmt.Println(message, details)
	}
	return &movies, nil
}

func UpdateCreateBulk(movies []dbd.Movie) string {
	CreateMovieBulk(movies)
	return "saved"
}

func UpdateMovie(movie dbd.Movie) string {
	err := Db.Save(&movie)

	if err.Error != nil {
		fmt.Println(err.Error)
		return "failed"
	}
	return "saved"
}

func CreateMovieBulk(movies []dbd.Movie) string {
	err := Db.Clauses(clause.OnConflict{
		UpdateAll: true,
	}).Create(&movies)

	if err == nil {
		return "Inserted"
	} else {
		fmt.Println(err.Error)
		return "Error"
	}
}

func MovieDestroy(id uint) {
	movie := new(dbd.Movie)
	movie.ID = id
	err := Db.Delete(&movie)

	if err != nil {
		fmt.Println(err.Error.Error())
	}
}


func ReadMovieByBoxOfficeID(boxOfficeID string) *dbd.Movie {
	var source *dbd.ExternalSource

	err := Db.Preload("Movies").Where("source = ? AND external_id = ?", "BoxOfficeMojo", boxOfficeID).Table("external_sources").First(&source)

	if err.Error != nil {
		fmt.Println("ERROR:", err.Error)
		return nil
	}

	if len(source.Movies) < 1 {
		return nil
	}
	return &source.Movies[0]
}

func movieMigrate() error {

	err := Db.AutoMigrate(&dbd.Movie{})
	if err != nil {
		fmt.Println(err)
		return err
	}
	return nil
}
