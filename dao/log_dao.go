package dao

import (
	"fmt"
	"movies-go/models/config"
	"time"
)

func CreateLog(log *config.Log) (*config.Log, error) {

	log.Timestamp = time.Now()
	result := Db.Create(&log)
	if result.Error != nil {
		fmt.Println(result.Error)
		return nil, result.Error
	} else if result.RowsAffected > 0 {
		fmt.Println(result.RowsAffected, "Log Added")
	}

	if result.Error != nil {
		fmt.Println(result.Error)
		return nil, result.Error
	}

	return log, nil
}

func ReadLogByID(LogID string) (*config.Log, error) {
	var log config.Log

	Db.Where(LogID).First(&log)

	return &log, nil
}


func ReadLogIndex() (*[]config.Log, error) {
	var logs []config.Log

	Db.Limit(100).Find(&logs)

	return &logs, nil
}

func logMigrate() error {

	err := Db.AutoMigrate(&config.Log{})
	if err != nil {
		fmt.Println(err)
		return err
	}
	return nil
}

func ReadLogsByType(logEvent string) (*[]config.Log, error) {
	var logs []config.Log

	Db.Limit(100).Where(&config.Log{LogEvent: logEvent}).Order("timestamp desc").Find(&logs)

	return &logs, nil
}
