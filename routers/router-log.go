package routers

import (
	"fmt"
	"github.com/gorilla/mux"
	"movies-go/dao"
	"movies-go/models/config"
	"net/http"
)

func StartLogRouter(r *mux.Router, basePath string) {
	var localModel = "log"
	initMethod(r, "GET", basePath, localModel, "", getLogIndexEndPoint)
	initMethod(r, "GET", basePath, localModel, "/", getLogIndexEndPoint)
	initMethod(r, "GET", basePath, localModel, "/{id}", getLogByIDEndPoint)
	initMethod(r, "GET", basePath, localModel + "/event", "/{event}", getLogsByTypeEndPoint)
}

func getLogIndexEndPoint(w http.ResponseWriter, r *http.Request) {
	logs, err := dao.ReadLogIndex()
	if err != nil {
		message := "[ Error Getting Log Index ]"
		details := err.Error()
		fmt.Println(message, details)
	}
	respondWithJson(w, r, http.StatusOK, logs)
}

func getLogByIDEndPoint(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	log, err := dao.ReadLogByID(vars["id"])
	if err != nil {
		message := "[ Error Get Log By ID ]"
		details := err.Error()
		fmt.Println(message, details)
		l := config.Log{}
		log = &l
	}
	respondWithJson(w, r, http.StatusOK, log)
}

func getLogsByTypeEndPoint(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	log, err := dao.ReadLogsByType(vars["event"])
	if err != nil {
		message := "[ Error Get Logs By Event ]"
		details := err.Error()
		fmt.Println(message, details)
	}
	respondWithJson(w, r, http.StatusOK, log)
}
