package routers

import (
	"fmt"
	"github.com/gorilla/mux"
	"movies-go/dao"
	"net/http"
	"strconv"
)

func StartNominationRouter(r *mux.Router, basePath string) {
	var localModel = "nomination"
	initMethod(r, "GET", basePath, localModel, "", getNominationIndexEndPoint)
	initMethod(r, "GET", basePath, localModel, "/", getNominationIndexEndPoint)
	initMethod(r, "GET", basePath, localModel, "/{id}", getNominationByIDEndPoint)
}

func getNominationIndexEndPoint(w http.ResponseWriter, r *http.Request) {
	nominations, err := dao.ReadNominationIndex()
	if err != nil {
		message := "[ Error Getting Nomination Index ]"
		details := err.Error()
		fmt.Println(message, details)
	}
	respondWithJson(w, r, http.StatusOK, nominations)
}

func getNominationByIDEndPoint(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id, err := strconv.Atoi(vars["id"])

	if err != nil {
		message := "[ Error Parsing ID for Nomination ]"
		details := err.Error()
		fmt.Println(message, details)
	}

	nomination, err := dao.ReadNominationByID(id)
	if err != nil {
		message := "[ Error Get Nomination By ID ]"
		details := err.Error()
		fmt.Println(message, details)
	}

	respondWithJson(w, r, http.StatusOK, nomination)
}
