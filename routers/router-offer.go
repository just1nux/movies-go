package routers

import (
	"fmt"
	"github.com/gorilla/mux"
	"movies-go/dao"
	"net/http"
	"strconv"
)

func StartOfferRouter(r *mux.Router, basePath string) {
	var localModel = "offer"
	initMethod(r, "GET", basePath, localModel, "", getOfferIndexEndPoint)
	initMethod(r, "GET", basePath, localModel, "/", getOfferIndexEndPoint)
	initMethod(r, "GET", basePath, localModel, "/{id}", getOfferByIDEndPoint)
}

func getOfferIndexEndPoint(w http.ResponseWriter, r *http.Request) {
	offers, err := dao.ReadOfferIndex()
	if err != nil {
		message := "[ Error Getting Offer Index ]"
		details := err.Error()
		fmt.Println(message, details)
	}
	respondWithJson(w, r, http.StatusOK, offers)
}

func getOfferByIDEndPoint(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id, err := strconv.Atoi(vars["id"])

	if err != nil {
		message := "[ Error Parsing ID for Offer ]"
		details := err.Error()
		fmt.Println(message, details)
	}

	offer, err := dao.ReadOfferByID(id)
	if err != nil {
		message := "[ Error Get Offer By ID ]"
		details := err.Error()
		fmt.Println(message, details)
	}

	respondWithJson(w, r, http.StatusOK, offer)
}
