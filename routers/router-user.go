package routers

import (
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"movies-go/dao"
	"movies-go/models/dbd"
	"net/http"
)

func StartUserRouter(r *mux.Router, basePath string) {
	var localModel = "user"
	initMethod(r, "GET", basePath, localModel, "", getUserIndexEndPoint)
	initMethod(r, "GET", basePath, localModel, "/", getUserIndexEndPoint)
	initMethod(r, "GET", basePath, localModel, "/{id}", getUserByIDEndPoint)
	initMethod(r, "PUT", basePath, localModel, "/update", updateUserByIDEndPoint)
	}

func getUserIndexEndPoint(w http.ResponseWriter, r *http.Request) {
	users, err := dao.ReadUserIndex()
	if err != nil {
		message := "[ Error Getting User Index ]"
		details := err.Error()
		fmt.Println(message, details)
	}
	respondWithJson(w, r, http.StatusOK, users)
}

func getUserByIDEndPoint(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	user, err := dao.ReadUserByID(vars["id"])
	if err != nil {
		message := "[ Error Get User By ID ]"
		details := err.Error()
		fmt.Println(message, details)
		u := dbd.User{}
		user = &u
	}
	respondWithJson(w, r, http.StatusOK, user)
}

func updateUserByIDEndPoint(w http.ResponseWriter, r *http.Request) {
	var user dbd.User
	if err := json.NewDecoder(r.Body).Decode(&user); err != nil {
		respondWithError(w, r, http.StatusOK, err.Error())
	} else {
		fmt.Println("RECEIVED", user)
		u, err := dao.CreateUser(&user)
		if err != nil {
			fmt.Println(err.Error())
		}
		respondWithJson(w, r, http.StatusOK, &u)
	}
}
