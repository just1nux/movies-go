package routers

import (
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"log"
	"movies-go/tools"
	"net/http"
	"strings"
	"time"
)

const (
	basePath = "/apis/"
	port = ":3600"
)

func StartServer() {
	r := mux.NewRouter()
	StartRouters(r)

	if err := http.ListenAndServe(port, r); err != nil {
		log.Fatal(err)
	}

}

func StartRouters(r *mux.Router){
	fmt.Println(">> Routers Port " + port)
	StartLogRouter(r, basePath)
	time.Sleep(100 * time.Millisecond)
	StartUserRouter(r, basePath)
	time.Sleep(100 * time.Millisecond)
	StartExternalSourceRouter(r, basePath)
	time.Sleep(100 * time.Millisecond)
	StartProviderRouter(r, basePath)
	time.Sleep(100 * time.Millisecond)
	StartNominationRouter(r, basePath)
	time.Sleep(100 * time.Millisecond)
	StartOfferRouter(r, basePath)
	time.Sleep(100 * time.Millisecond)
	StartMovieRouter(r, basePath)
	fmt.Println(">> Routers")
}

func respondWithError(w http.ResponseWriter, r *http.Request, code int, msg string) {
	respondWithJson(w, r, code, map[string]string{"error": msg})
}

func respondWithJson(w http.ResponseWriter, r *http.Request, code int, payload interface{}) {

	if !strings.Contains(r.URL.Path, "/apis/") {
		w, payload = tools.TokenCheck(w, r, payload)
	}
	response, _ := json.Marshal(payload)
	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Access-Control-Expose-Headers", "Authorization")
	w.WriteHeader(code)
	_, _ = w.Write(response)
}

func initMethod(r *mux.Router, method string, basePath string, localModel string,
	params string, f func(http.ResponseWriter, *http.Request)) {
	url := basePath + localModel + params
	r.HandleFunc(url, f).Methods(method)
	fmt.Printf("  -- Path Active: '%s' by '%s'\n", url,  method)
}
