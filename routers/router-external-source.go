package routers

import (
	"fmt"
	"github.com/gorilla/mux"
	"movies-go/dao"
	"net/http"
	"strconv"
)

func StartExternalSourceRouter(r *mux.Router, basePath string) {
	var localModel = "external_source"
	initMethod(r, "GET", basePath, localModel, "", getExternalSourceIndexEndPoint)
	initMethod(r, "GET", basePath, localModel, "/", getExternalSourceIndexEndPoint)
	initMethod(r, "GET", basePath, localModel, "/{id}", getExternalSourceByIDEndPoint)
}

func getExternalSourceIndexEndPoint(w http.ResponseWriter, r *http.Request) {
	externalSources, err := dao.ReadExternalSourceIndex()
	if err != nil {
		message := "[ Error Getting External Source Index ]"
		details := err.Error()
		fmt.Println(message, details)
	}
	respondWithJson(w, r, http.StatusOK, externalSources)
}

func getExternalSourceByIDEndPoint(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id, err := strconv.Atoi(vars["id"])

	if err != nil {
		message := "[ Error Parsing ID forExternal Source ]"
		details := err.Error()
		fmt.Println(message, details)
	}

	externalSource, err := dao.ReadExternalSourceByID(id)
	if err != nil {
		message := "[ Error Get External Source By ID ]"
		details := err.Error()
		fmt.Println(message, details)
	}

	respondWithJson(w, r, http.StatusOK, externalSource)
}
