package routers

import (
	"fmt"
	"github.com/gorilla/mux"
	"movies-go/dao"
	"net/http"
	"strconv"
)

func StartProviderRouter(r *mux.Router, basePath string) {
	var localModel = "provider"
	initMethod(r, "GET", basePath, localModel, "", getProviderIndexEndPoint)
	initMethod(r, "GET", basePath, localModel, "/", getProviderIndexEndPoint)
	initMethod(r, "GET", basePath, localModel, "/{id}", getProviderByIDEndPoint)
}

func getProviderIndexEndPoint(w http.ResponseWriter, r *http.Request) {
	providers, err := dao.ReadProviderIndex()
	if err != nil {
		message := "[ Error Getting Provider Index ]"
		details := err.Error()
		fmt.Println(message, details)
	}
	respondWithJson(w, r, http.StatusOK, providers)
}

func getProviderByIDEndPoint(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id, err := strconv.Atoi(vars["id"])

	if err != nil {
		message := "[ Error Parsing ID for Provider ]"
		details := err.Error()
		fmt.Println(message, details)
	}

	provider, err := dao.ReadProviderByID(id)
	if err != nil {
		message := "[ Error Getting Provider By ID ]"
		details := err.Error()
		fmt.Println(message, details)
	}

	respondWithJson(w, r, http.StatusOK, provider)
}
