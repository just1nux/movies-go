package routers

import (
	"fmt"
	"github.com/gorilla/mux"
	"movies-go/dao"
	"movies-go/tools"
	"net/http"
	"strconv"
)

func StartMovieRouter(r *mux.Router, basePath string) {
	var localModel = "movie"
	initMethod(r, "GET", basePath, localModel, "", getMovieIndexEndPoint)
	initMethod(r, "GET", basePath, localModel, "/", getMovieIndexEndPoint)
	initMethod(r, "GET", basePath, localModel, "/{id}", getMovieByIDEndPoint)
}

func getMovieIndexEndPoint(w http.ResponseWriter, r *http.Request) {
	movies, err := dao.ReadMovieIndex()

	if err != nil {
		message := "[ Error Getting Movie Index ]"
		details := err.Error()
		tools.LogIt("ERROR", message, details)
	}
	respondWithJson(w, r, http.StatusOK, movies)
}

func getMovieByIDEndPoint(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id, err := strconv.Atoi(vars["id"])

	if err != nil {
		message := "[ Error Parsing ID for Movie ]"
		details := err.Error()
		fmt.Println(message, details)
	}

	movie, err := dao.ReadMovieByID(id)
	if err != nil {
		message := "[ Error Get Movie By ID ]"
		details := err.Error()
		fmt.Println(message, details)
	}

	respondWithJson(w, r, http.StatusOK, movie)
}
