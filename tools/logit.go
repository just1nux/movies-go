package tools

import (
	"fmt"
	"movies-go/dao"
	"movies-go/models/config"
	"time"
)

func LogIt(eventType string, message string, details string){
	currentTime := time.Now()
	logItem := config.Log{
		Timestamp:currentTime,
		EventType: eventType,
		Message:message,
		Details:details}
	log, err := dao.CreateLog(&logItem)

	if err != nil {
		fmt.Println("Error Logging:", err)
		return
	}

	fmt.Println("[ ", log.Timestamp,"Logged:", log.EventType, log.Message, log.Details, "]")
}
