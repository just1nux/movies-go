package tools

import (
	"encoding/json"
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"movies-go/models/config"
	"movies-go/models/dbd"
	"net/http"
	"strings"
	"time"
)

var jwtKey = []byte("mvcAaqtEJkuX3qGfsOD_YQLmfbgJMHWT9v1uxCVxMghg_dkjlSDq-yN0k4yrZyB2sbbmDHCKbTVMLuZjSJPSkRYLxrlpO-xlDDFuhTAibgyGXEDjVxG4nXnTHdaxnLqOtKm2d1orhMdFc9DR636wHc9BkjOVTZhi7HwE8fuuL0yp1MNth1FbEwElZDDuSzf4O9yHvX_-fMDp-BJX-xebJZ-ky4WPU9RXbhW8mmw5QZ7QNKD4mxERDWlA2xkGPz1WnJsJKJVSCqazWFFZXH0S_-GuYMHuepVfnRTe1Fvc1vHiX9fjHLGSfy39bJ4sEbFdgRfqkSaBIqC3p2msXBLgWQ")
const expireMinutes = 60

type Claims struct {
	Username string `json:"username"`
	jwt.StandardClaims
}

type Box struct {

}

func CreateJWT(course string, user *dbd.User, w http.ResponseWriter, r http.Request) (config.JWT, http.ResponseWriter, http.Request) {

	expirationTime := time.Now().Add(expireMinutes * time.Minute)

	// make the claim
	claims := &Claims{
		Username: user.LoginID,
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: expirationTime.Unix(),
		},
	}

	// Declare the token with the algorithm used for signing, and the claims
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	// Create the JWT string
	tokenString, err := token.SignedString(jwtKey)
	if err != nil {
		// If there is an error in creating the JWT return an internal server error
		w.WriteHeader(http.StatusInternalServerError)
		jwtTokenNil := config.JWT{}
		return jwtTokenNil, w, r
	}

	jwtTok := config.JWT{
		Name:    "token",
		Value:   tokenString,
		ExpirationTime: expirationTime,
		Path: "/",
		Course: course,
		User: *user,
	}

	m, _ := json.Marshal(jwtTok)
	w.Header().Del("Authorization")
	w.Header().Add("Authorization", "API Key " + string(m))
	r.Header.Del("Authorization")
	r.Header.Add("Authorization", "API Key " + string(m))
	return jwtTok, w, r
}

func RefreshJWT(w http.ResponseWriter, r *http.Request) http.ResponseWriter {
	var jwtObject config.JWT

	reqToken := r.Header.Get("Authorization")
	splitToken := strings.Split(reqToken, "API Key")
	if len(splitToken) > 1 {
		reqToken = splitToken[1]
	}

	err := json.Unmarshal([]byte(reqToken), &jwtObject)

	claims := &Claims{}
	_, err = jwt.ParseWithClaims(jwtObject.Value, claims, func(token *jwt.Token) (interface{}, error) {
		return jwtKey, nil
	})
	// Now, create a new token for the current use, with a renewed expiration time
	expirationTime := time.Now().Add(expireMinutes * time.Minute)
	claims.ExpiresAt = expirationTime.Unix()
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	tokenString, err := token.SignedString(jwtKey)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return nil
	}

	// fmt.Println("TOKEN REFRESH TO: ", time.Unix(claims.ExpiresAt, 0).String())

	jwtObject.Value = tokenString
	jwtObject.ExpirationTime = time.Unix(claims.ExpiresAt, 0)
	m, _ := json.Marshal(jwtObject)
	w.Header().Del("Authorization")
	w.Header().Add("Authorization", "API Key " + string(m))
	return w
}


func CheckJWT(_ http.ResponseWriter, r *http.Request) bool {
	var jwtObject config.JWT

	reqToken := r.Header.Get("Authorization")
	splitToken := strings.Split(reqToken, "API Key")
	if len(splitToken) > 1 {
		reqToken = splitToken[1]
	}

	err := json.Unmarshal([]byte(reqToken), &jwtObject)

	if err != nil {
		fmt.Println("CheckJWT - Failure to unmarshal' header", err.Error())
		return false
	}

	claims := &Claims{}
	tkn, err := jwt.ParseWithClaims(jwtObject.Value, claims, func(token *jwt.Token) (interface{}, error) {
		return jwtKey, nil
	})
	if err != nil {
		fmt.Println("Error B", err.Error())
		if err == jwt.ErrSignatureInvalid {
			return false
		}
		return false
	}
	if !tkn.Valid {
		return false
	}

	if time.Unix(claims.ExpiresAt, 0).Sub(time.Now()) < 0 {
		return false
	}
	return true
}

func TokenCheck(w http.ResponseWriter, r *http.Request, p interface{}) (http.ResponseWriter, *interface{}) {

	if CheckJWT(w, r) {
		w = RefreshJWT(w, r)
	} else {
		w.Header().Del("Authorization")
		w.Header().Set("Authorization","")
		p = nil
	}

	return w, &p
}

func GetUser(r *http.Request) *dbd.User {
	var jwtObject config.JWT

	reqToken := r.Header.Get("Authorization")
	splitToken := strings.Split(reqToken, "API Key")
	if len(splitToken) > 1 {
		reqToken = splitToken[1]
	}

	err := json.Unmarshal([]byte(reqToken), &jwtObject)
	if err != nil {
		fmt.Println("getUser - Failure to unmarshal' header", err.Error())
		return nil
	}
	return &jwtObject.User
}
