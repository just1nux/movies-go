package jw

type JWMedia struct {
	Title                         string      `bson:"Title,omitempty" json:"title"`
	FullPath                      string      `bson:"FullPath,omitempty" json:"full_path"`
	FullPaths                     JWFullPaths `bson:"FullPaths,omitempty" json:"full_paths"`
	Poster                        string      `bson:"Poster,omitempty" json:"poster"`
	ShortDescription              string      `bson:"ShortDescription,omitempty" json:"short_description"`
	OriginalReleaseYear           int         `bson:"OriginalReleaseYear,omitempty" json:"original_release_year"`
	TmdbPopularity                float64     `bson:"TmdbPopularity,omitempty" json:"tmdb_popularity"`
	ObjectType                    string      `bson:"ObjectType,omitempty" json:"object_type"`
	OriginalTitle                 string      `bson:"OriginalTitle,omitempty" json:"original_title"`
	LocalizedReleaseDate          string      `bson:"LocalizedReleaseDate,omitempty" json:"localized_release_date"`
	OriginalLanguage              string      `bson:"OriginalLanguage,omitempty" json:"original_language"`
	AgeCertification              string      `bson:"AgeCertification,omitempty" json:"age_certification"`
	CinemaReleaseDate             string      `bson:"Runtime,omitempty" json:"cinema_release_date"`
	CinemaReleaseWeek             string      `bson:"Runtime,omitempty" json:"cinema_release_week"`
	Runtime                       int         `bson:"Runtime,omitempty" json:"runtime"`
	IsNationwideCinemaReleaseDate bool        `bson:"IsNationwideCinemaReleaseDate,omitempty" json:"is_nationwide_cinema_release_date"`
	Offers                        []JWOffer   `bson:"Offers,omitempty" json:"offers"`
	Scoring                       []JWScoring `bson:"Scoring,omitempty" json:"scoring"`
}

type JWScoring struct {
	Scoring			string 	`bson:"Scoring,omitempty" json:scoring"`
	ProviderType	string 	`bson:"ProviderType,omitempty" json:"provider_type"`
	Value			float64 	`bson:"Value,omitempty" json:"value"`
}

type JWOffer struct {
	MonetizationType			string 		`bson:"MovieDetailCinema,omitempty" json:"monetization_type"`
	ProviderId					int 		`bson:"MovieDetailOverview,omitempty" json:"provider_id"`
	RetailPrice					float64		`bson:"RetailPrice,omitempty" json:"retail_price"`
	LastChangeRetailPrice		float64		`bson:"LastChangeRetailPrice,omitempty" json:"last_change_retail_price"`
	LastChangeDifference		float64		`bson:"LastChangeDifference,omitempty" json:"last_change_detail"`
	LastChangePercent			float64		`bson:"LastChangePercent,omitempty" json:"last_change_percentage"`
	LastChangeDate				string		`bson:"LastChangeDate,omitempty" json:"last_change_date"`
	LastChangeDateProviderId	string		`bson:"LastChangeDateProviderId,omitempty" json:"last_change_date_provider_id"`
	Currency					string		`bson:"Currency,omitempty" json:"currency"`
	SubtitleLanguage			[]string	`bson:"SubtitleLanguage,omitempty" json:"subtitle_languages"`
	PresentationType			string		`bson:"PresentationType,omitempty" json:"presentation_type"`
	DateCreatedProviderId		string		`bson:"DateCreatedProviderId,omitempty" json:"date_created_provider_id"`
	DateCreated					string		`bson:"DateCreated,omitempty" json:"date_created"`
	Country						string		`bson:"Country,omitempty" json:"country"`
}

type JWFullPaths struct {
	MovieDetailCinema	       string 		`json:"MOVIE_DETAIL_CINEMA"`
	MovieDetailOverview	       string 	    `json:"MOVIE_DETAIL_OVERVIEW"`
}

type Payload struct {
	AgeCertification 			[]string 	`json:"age_certifications"`
	ContentTypes 				[]string 	`json:"content_types"`
	PresentationTypes			[]string 	`json:"presentation_types"`
	Providers 					[]string 	`json:"providers"`
	Genres 						[]string 	`json:"genres"`
	ReleaseYearFrom 			int         `json:"release_year_from"`
	ReleaseYearUntil 			int        `json:"release_year_until"`
	MonetizationType			[]string   `json:"monetization_types"`
	MinPrice					int        `bson:"MinPrice,omitempty" json:"min_price"`
	MaxPrice					int        `json:"max_price"`
	NationwideCinemaRelease		bool    `json:"nationwide_cinema_releases_only"`
	ScoringFilterType			[]string  `json:"scoring_filter_type"`
	CinemaRelease				string    `json:"cinema_release"`
	Query						string    `json:"query"`
	TimelineTypes				string    `json:"timeline_types"`
	Items						[]JWMedia `json:"items"`
	Page						int        `json:"page"`
	PageSize					int        `json:"page_size"`
	TotalPages					int      `json:"total_pages"`
	TotalResults				int        `json:"total_results"`
}
