package dbd

import (
	"gorm.io/gorm"
	"time"
)

type Media struct {
	gorm.Model
	MediaId			int         `json:"mediaId"`
	MediaListId		int         `json:"mediaListId"`
	DatesPlayed     []time.Time `json:"datesPlayed"`
	SoloPlay		bool        `json:"soloPlay"`
	Owned			bool        `json:"owned"`
	Movie		    *Movie      `json:"mediaItem,omitempty"`
}

type MediaList struct {
	gorm.Model
	UserId       int      `json:"user_id"`
	ListName	 string   `json:"list_name" gorm:"size:256"`
	Favorite	 bool     `json:"favorite"`
	Media        []Media  `json:"media"`
}

type MediaUpdate struct {
	MediaMeta   Media `json:"mediaMeta"`
	MediaList   MediaList `json:"mediaList"`
}
