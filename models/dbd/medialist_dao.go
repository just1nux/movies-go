package dbd

func GetMediaListsByUserId(id string) []MediaList {
	var mediaList []MediaList
	/* client := GetClient()
	err := client.Connect(context.Background())
	c := client.Database("movies").Collection("mediaLists")

	oid, err := primitive.ObjectIDFromHex(id)

	if err != nil {
		fmt.Println(err)
	}

	var mediaList []models.MediaList

	pipeline := []bson.M{
		{"$match": bson.M{ "_user_id": oid}},
		{"$sort": bson.M{ "Favorite": -1, "ListName": 1 }},
	}
	cur, err := c.Aggregate(context.Background(), pipeline)
	if err != nil {
		fmt.Println(err)
	}

	for cur.Next(context.TODO()) {
		var elem models.MediaList
		err := cur.Decode(&elem)
		if err != nil {
			fmt.Println(err.Error())
			log.Fatal(err)
		}

		mediaList = append(mediaList, elem)
	}

	err = cur.Close(context.TODO())
	if err != nil {
		fmt.Println(err)
	}

	err = client.Disconnect(context.Background())
	if err != nil {
		fmt.Println(err.Error())
	}*/

	return mediaList
}


func GetMediaByListId(ask MediaAsk) MediaAsk {
	/*client := GetClient()
	err := client.Connect(context.TODO())
	c := client.Database("movies").Collection("mediaLists")

	oid, err := primitive.ObjectIDFromHex(ask.SearchQuery)
	if err != nil {
		fmt.Println(err)
	}

	var mediaList models.MediaList

	var pipeline []bson.M

	pipeline = append(pipeline, bson.M{"$match": bson.M{"_id": oid}})

	pipeline = append(pipeline, bson.M{"$lookup": bson.M{
		"from": "mediaMetas",
		"localField": "_id","foreignField": "MediaListId",
		"as": "MediaMetas"}})

	pipeline = append(pipeline, bson.M{"$unwind": bson.M{
		"path": "$MediaMetas",
		"preserveNullAndEmptyArrays": true,
	}})

	for _, filter := range ask.Filters {
		m :=  models.GetBson(filter)
		if len(m) > 0 {
			pipeline = append(pipeline,m)
		}
	}

	pipeline = append(pipeline, bson.M{"$lookup": bson.M{
		"from": "movies",
		"localField": "MediaMetas.MediaId","foreignField": "_id",
		"as": "MediaMetas.MediaItem"}})

	pipeline = append(pipeline, bson.M{"$unwind": bson.M{
		"path": "$MediaMetas.MediaItem",
		"preserveNullAndEmptyArrays": true,
	}})

	pipeline = append(pipeline, bson.M{"$project": bson.M{
		"_user_id":1,
		"Favorite":1,
		"ListName":1,
		"MediaMetas":1,
		"ExternalSources": "$MediaMetas.MediaItem.ExternalSources",
	}})

	pipeline = append(pipeline, bson.M{"$unwind": bson.M{
		"path": "$ExternalSources",
		"preserveNullAndEmptyArrays": true,
	}})

	pipeline = append(pipeline, bson.M{"$match": bson.M{
		"ExternalSources.Source":"BoxOfficeMojo",
	}})

	sortOrder := -1
	sortType := "MediaMetas.MediaItem.Title"

	if ask.Sort.Value == true {
		sortOrder = 1
	}

	if ask.Sort.Attribute == "boxoffice" {
		sortType = "ExternalSources.Score"
	}

	pipeline = append(pipeline, bson.M{"$sort": bson.M{sortType: sortOrder}})

	pipeline = append(pipeline, bson.M{"$group": bson.M{
		"_id":        "$_id",
		"MediaMetas": bson.M{
				"$push":
				"$MediaMetas"},
		"ListName":   bson.M{"$first": "$ListName"},
		"_user_id":   bson.M{"$first": "$_user_id"},
		"Favorite":	  bson.M{"$first": "$Favorite"},
	}})


	pipeline = append(pipeline, bson.M{"$project": bson.M{
		"_id":      "$_id",
		"ListName": "$ListName",
		"_user_id": "$_user_id",
		"Favorite": "$Favorite",
		"MediaMetas": bson.M{
			"$cond": []interface{}{
				bson.M{
					"$eq": []interface{}{[]interface{}{bson.D{}}, "$MediaMetas"}},
				nil,
				"$MediaMetas",
			},
		},
	}})

	cur, err := c.Aggregate(context.Background(), pipeline)
	if err != nil {
		fmt.Println(err)
	}
	if cur.Next(context.TODO()){
		// fmt.Println(cur.Current.String())
		err = cur.Decode(&mediaList)
	}
	//fmt.Println(mediaList)

	err = cur.Close(context.TODO())
	if err != nil {
		fmt.Println(err)
	}

	err = client.Disconnect(context.Background())
	if err != nil {
		fmt.Println(err.Error())
	}

	ask.MediaList = mediaList
	err = cur.Close(context.TODO())
	err = client.Disconnect(context.Background())
	if err != nil {
		fmt.Println(err.Error())
	}
*/
	return ask
}

func CreateMediaList(mediaList MediaList) string {

	/* client := GetClient()
	err := client.Connect(context.Background())
	c := client.Database("movies").Collection("mediaLists")

	mediaList.Id = primitive.NewObjectID()
	_, err = c.InsertOne(context.TODO(), mediaList)

	err = client.Disconnect(context.Background())
	if err != nil {
		fmt.Println(err.Error())
	}

	if err == nil {
		return mediaList.Id.Hex()
	} else {
		return "error"
	}

	 */
	return ""
}

func UpdateMediaList(mediaList MediaList) string {
	var err string
/*
	client := GetClient()
	err := client.Connect(context.Background())
	c := client.Database("movies").Collection("mediaLists")


	_ = c.FindOneAndReplace(context.TODO(), bson.M{"_id": mediaList.Id}, mediaList)

	err = client.Disconnect(context.Background())

	if err == nil {
		return mediaList.Id.Hex()
	} else {
		return err.Error()
	}
}

func DeleteMediaList(id string) error {

	//Remove all metas for this list.
	DeleteMetasByMediaList(id)

	client := GetClient()
	err := client.Connect(context.Background())
	c := client.Database("movies").Collection("mediaLists")

	oid, err := primitive.ObjectIDFromHex(id)

	if err != nil {
		fmt.Println(err)
	}
	result := c.FindOneAndDelete(context.TODO(), bson.M{"_id":oid})

	err = client.Disconnect(context.Background())
	if err != nil {
		fmt.Println(err.Error())
	}
*/
	return err
}

