package dbd

import (
	"gorm.io/gorm"
	"time"
)

type Movie struct {
	gorm.Model
	Title 			string 				`json:"title" gorm:"size:256"`
	Description 	string 				`json:"description" gorm:"size:256"`
	Released  		time.Time			`json:"released"`
	Studio			string 				`json:"studio" gorm:"size:64"`
	Nominations 	[]Nomination 		`json:"nominations" gorm:"many2many:movie_nomination"`
	ExternalSources []ExternalSource 	`json:"externalSources" gorm:"many2many:movie_external_source"`
	Offers			[]Offer 			`json:"offers" gorm:"many2many:movie_offer"`
}

type ExternalSource struct {
	gorm.Model
	ExternalID   string 		 `json:"external_id" gorm:"size:32"`
	Source  	 string 		 `json:"source" gorm:"size:16"`
	Score        int 			 `json:"score"`
	Movies		 []Movie	     `json:"movies" gorm:"many2many:movie_external_source"`
}

type Nomination struct {
	gorm.Model
	Body   		string 		 `json:"body" gorm:"size:64"`
	Award  		string 		 `json:"award" gorm:"size:64"`
	Category   	string 		 `json:"category"  gorm:"size:64"`
	Winner 		bool   		 `json:"winner"`
	Year		string 		 `json:"year"  gorm:"size:8"`
}

type Offer struct {
	gorm.Model
	MonetizationType     string         			 `json:"monetization_type" gorm:"size:16"`
	Providers            []Provider     			 `json:"providers" gorm:"many2many:offer_provider"`
	RetailPrice          float64         			 `json:"retail_price"`
	PresentationTypes	 []PresentationType  		 `json:"presentation_types" gorm:"many2many:provider_presentation_type"`
}

type Provider struct {
	gorm.Model
	IdJW 						int		 					 `json:"id_jw,omitempty"`
	TechnicalName 				string 						 `json:"technical_name" gorm:"size:256"`
	ShortName					string 						 `json:"short_name" gorm:"size:256"`
	ClearName 					string 						 `json:"clear_name" gorm:"size:256"`
	IconURL		 				string 						 `json:"icon_url" gorm:"size:256"`
	Slug		 				string 						 `json:"slug" gorm:"size:2048"`
	ExternalSources 			[]ExternalSource 			 `json:"externalSources" gorm:"many2many:provider_external_source"`
	MonetizationTypes	        []MonetizationType		     `json:"monetization_types" gorm:"many2many:provider_monetization_type"`
	PresentationTypes	        []PresentationType  		 `json:"presentation_types" gorm:"many2many:provider_presentation_type"`
}

type MonetizationType struct {
	gorm.Model
	Type 						string 			 `json:"type,omitempty" gorm:"size:16"`
}

type PresentationType struct {
	gorm.Model
	Type 						string 			 `json:"type,omitempty" gorm:"size:16"`
}

