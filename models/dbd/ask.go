package dbd

import "gorm.io/gorm"

type MediaAsk struct {
	gorm.Model
	ReleaseYear  		int			`json:"release_year"`
	SearchQuery  		string 		`json:"search_query" gorm:"size:64"`
	SearchParam  		string 		`json:"search_param"  gorm:"size:32"`
	Page  		 		int 		`json:"page"`
	PageSize     		int 		`json:"page_size"`
	TotalPages			int	 	 	`json:"total_pages"`
	TotalResults		int	 	 	`json:"total_results"`
	Sort				Sort 		`json:"sort"`
	Filters				[]Filter	`json:"filters"`
	MediaListID			MediaList	`json:"media_list_id"`
	MediaList			MediaList	`json:"media_List"`
}

type Filter struct {
	gorm.Model
	Attribute  			string 		`json:"_attribute" gorm:"size:16"`
	Operator			string 		`json:"_operator" gorm:"size:16"`
	Value				bool 		`json:"_value"`
}

type Sort struct {
	gorm.Model
	Attribute  			string 		`json:"_attribute" gorm:"size:16"`
	Operator			string 		`json:"_operator" gorm:"size:16"`
	Value				bool 		`json:"_value"`
}
