package dbd

import (
	"gorm.io/gorm"
	"time"
)

type User struct {
	gorm.Model
	CanvasID     int         `json:"canvas_id"`
	Name         string      `json:"name,omitempty" gorm:"size:100"`
	ShortName    string      `json:"short_name,omitempty" gorm:"size:100"`
	SortableName string      `json:"sortable_name,omitempty" gorm:"size:100"`
	TimeZone     string      `json:"time_zone,omitempty" gorm:"size:25"`
	Locale       string      `json:"locale,omitempty" gorm:"size:50"`
	SISLoginID   string      `json:"sis_login_id,omitempty" gorm:"size:50"`
	LoginID      string      `json:"login_id" gorm:"size:50"`
	Birthdate    string      `json:"birthdate,omitempty" gorm:"size:50"`
	Email        string      `json:"email,omitempty" gorm:"size:75"`
	AvatarURL    string      `json:"avatar_url,omitempty" gorm:"size:200"`
	SISUserID    string      `json:"sis_user_id" gorm:"size:10"`
	Role         string      `json:"role" gorm:"size:10"`
	LastAccess	 time.Time   `json:"last_access"`
}
