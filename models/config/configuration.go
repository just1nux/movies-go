package config

type Configuration struct {
	DB     DB      `json:"databases"`
	Events []Event `json:"events"`
}
