package config

import (
	"movies-go/models/dbd"
	"time"
)

type JWT struct {
	Name           string    `json:"name,omitempty"`
	Value          string    `json:"value,omitempty"`
	ExpirationTime time.Time `json:"expirationTime,omitempty"`
	Path           string    `json:"path,omitempty"`
	Course         string    `json:"course,omitempty"`
	User           dbd.User  `json:"user,omitempty"`
}
