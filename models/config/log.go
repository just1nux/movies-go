package config

import (
	"gorm.io/gorm"
	"time"
)

type Log struct {
	gorm.Model
	LogEvent  string    `json:"logEvent,omitempty" gorm:"size:50"`
	EventType string    `json:"eventType,omitempty" gorm:"size:50"`
	Message   string    `json:"message,omitempty" gorm:"size:150"`
	Details   string    `json:"details,omitempty" gorm:"size:150"`
	Timestamp time.Time `json:"timestamp,omitempty" gorm:"size:75"`
}
