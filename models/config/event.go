package config

type Event struct {
	EventName        string    `json:"event_name,omitempty"`
	Channel          chan bool `json:"channel,omitempty"`
	State            State     `json:"state,omitempty"`
	RunHour          int       `json:"run_hour,omitempty"`
	RunMinute        int       `json:"run_minute,omitempty"`
	RepeatEveryHours float64   `json:"repeat_every_hours,omitempty"`
}
