package config

type State int

type EventState struct {
	Message string `json:"message"`
	State   State  `json:"state"`
}
