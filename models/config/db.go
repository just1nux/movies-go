package config

type DB struct {
	Server       string   `json:"server"`
	Login 		 string   `json:"login"`
	Password     string   `json:"password"`
	Database     string   `json:"database"`
}

