package main

import (
	"movies-go/daemon"
	"movies-go/dao"
	"movies-go/routers"
)

func main() {
	 dao.GetConfigFromFile()
	 _ = dao.Migrate()
	 daemon.Start()
	 routers.StartServer()
	}

